import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {SchoolEventService} from "../service/school-event.service";

@Component({
  selector: 'app-school-event',
  templateUrl: './school-event.component.html',
  styleUrls: ['./school-event.component.css']
})
export class SchoolEventComponent implements OnInit {

  newEventForm = new FormGroup({
    name: new FormControl(''),
  });


  constructor(private schoolEventService: SchoolEventService) { }

  ngOnInit(): void {
  }

  saveEvent() {
    const name = this.newEventForm.get('name')?.value;
    const url = 'http://127.0.0.1:8000/api/event'
    this.schoolEventService.postRequest(url, name)
      .subscribe(
        res => {
          console.log(res)
        },
        error => {}
      );

  }
}
