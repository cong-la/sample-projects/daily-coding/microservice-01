import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SchoolInfoComponent } from './school-info/school-info.component';
import { SchoolEventComponent } from './school-event/school-event.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    SchoolInfoComponent,
    SchoolEventComponent
  ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
      HttpClientModule,

    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
