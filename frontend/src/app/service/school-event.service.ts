import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SchoolEventService {

  constructor(protected httpClient: HttpClient) { }

  public postRequest(requestUrl: string, name: string) {
    return this.httpClient.post(requestUrl,
      {
        name,
      });
  }

}
