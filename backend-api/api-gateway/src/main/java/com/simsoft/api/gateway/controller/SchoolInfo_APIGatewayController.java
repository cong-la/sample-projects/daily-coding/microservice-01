package com.simsoft.api.gateway.controller;

import com.simsoft.api.gateway.service.schoolinfo.config.SchoolInfoMessageConfig;
import com.simsoft.api.gateway.service.schoolinfo.model.SchoolInfo;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "/api/school")
public class SchoolInfo_APIGatewayController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private DirectExchange findAllSchoolsExchange;

    @Autowired
    private DirectExchange findSchoolByIdExchange;

    @Autowired
    private DirectExchange addNewSchoolExchange;

    @GetMapping()
    public Object findAll() {
        return this.rabbitTemplate.convertSendAndReceive(findAllSchoolsExchange.getName(), SchoolInfoMessageConfig.SCHOOL_INFO_ROUTING_KEY, "findAll");
    }

    @GetMapping(path = "/{id}")
    public Object findById(@PathVariable int id) {
        return this.rabbitTemplate.convertSendAndReceive(findSchoolByIdExchange.getName(), SchoolInfoMessageConfig.FIND_SCHOOL_INFO_BY_ID_ROUTING_KEY, id);
    }

    @PostMapping()
    public Object save(@RequestBody SchoolInfo schoolInfo) {
        return this.rabbitTemplate.convertSendAndReceive(addNewSchoolExchange.getName(), SchoolInfoMessageConfig.ADD_NEW_SCHOOL_INFO_ROUTING_KEY, schoolInfo);
    }
}
