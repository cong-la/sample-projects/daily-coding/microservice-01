package com.simsoft.api.gateway.service.schoolinfo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SchoolInfoMessageConfig {

    //  FIND ALL
    public static final String SCHOOL_INFO_QUEUE = "SCHOOL_INFO_QUEUE";
    public static final String SCHOOL_INFO_EXCHANGE = "SCHOOL_INFO_EXCHANGE";
    public static final String SCHOOL_INFO_ROUTING_KEY = "SCHOOL_INFO_ROUTING_KEY";

    //  FIND_BY_ID
    public static final String FIND_SCHOOL_INFO_BY_ID_QUEUE = "FIND_SCHOOL_INFO_BY_ID_QUEUE";
    public static final String FIND_SCHOOL_INFO_BY_ID_EXCHANGE = "FIND_SCHOOL_INFO_BY_ID_EXCHANGE";
    public static final String FIND_SCHOOL_INFO_BY_ID_ROUTING_KEY = "FIND_SCHOOL_INFO_BY_ID_ROUTING_KEY";

    //  ADD NEW SCHOOL
    public static final String ADD_NEW_SCHOOL_INFO_QUEUE = "ADD_NEW_SCHOOL_INFO_QUEUE";
    public static final String ADD_NEW_SCHOOL_INFO_EXCHANGE = "ADD_NEW_SCHOOL_INFO_EXCHANGE";
    public static final String ADD_NEW_SCHOOL_INFO_ROUTING_KEY = "ADD_NEW_SCHOOL_INFO_ROUTING_KEY";

    //  QUEUE
    @Bean
    public Queue findAllSchoolsQueue() {
        return new Queue(SCHOOL_INFO_QUEUE);
    }

    @Bean
    public Queue findSchoolByIdQueue() {
        return new Queue(FIND_SCHOOL_INFO_BY_ID_QUEUE);
    }

    @Bean
    public Queue addNewSchoolQueue() {
        return new Queue(ADD_NEW_SCHOOL_INFO_QUEUE);
    }

    //  EXCHANGE
    @Bean
    public DirectExchange findAllSchoolsExchange() {
        return new DirectExchange(SCHOOL_INFO_EXCHANGE);
    }

    @Bean
    public DirectExchange findSchoolByIdExchange() {
        return new DirectExchange(FIND_SCHOOL_INFO_BY_ID_EXCHANGE);
    }

    @Bean
    public DirectExchange addNewSchoolExchange() {
        return new DirectExchange(ADD_NEW_SCHOOL_INFO_EXCHANGE);
    }

    //  BINDING
    @Bean
    public Binding findAllSchoolsBinding(Queue findAllSchoolsQueue, DirectExchange findAllSchoolsExchange) {
        return BindingBuilder.bind(findAllSchoolsQueue).to(findAllSchoolsExchange).with(SCHOOL_INFO_ROUTING_KEY);
    }

    @Bean
    public Binding findSchoolByIdBinding(Queue findSchoolByIdQueue, DirectExchange findSchoolByIdExchange) {
        return BindingBuilder.bind(findSchoolByIdQueue).to(findSchoolByIdExchange).with(FIND_SCHOOL_INFO_BY_ID_ROUTING_KEY);
    }

    @Bean
    public Binding addNewSchoolBinding(Queue addNewSchoolQueue, DirectExchange addNewSchoolExchange) {
        return BindingBuilder.bind(addNewSchoolQueue).to(addNewSchoolExchange).with(ADD_NEW_SCHOOL_INFO_ROUTING_KEY);
    }

}
