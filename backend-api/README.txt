#. Create docker network:
docker network create docker-microservice-network

#. For "package does not exist" issue:
run compile in Maven window.

1. Start MySQL:
docker run -p 3307:3306 --name docker-mysql --cap-add=sys_nice -e MYSQL_ROOT_USER=root -e MYSQL_ROOT_PASSWORD=123456789 -e MYSQL_DATABASE=school_database --net docker-microservice-network mysql

2. Start RabbitMQ:
docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 --net docker-microservice-network rabbitmq:3.9-management

3. Start API gateway:
docker build -t api-gateway .
docker run -p 8000:8000 --name api-gateway --env-file ../env/env-file.txt --net docker-microservice-network api-gateway

3.1. Push to docker hub:
docker tag e58946130b4e congla/daily-coding:api-gateway
# e58946130b4e: the image's ID.
# congla: docker username.
# daily-coding: docker repository.
# api-gateway: the image's name.
docker push congla/daily-coding:api-gateway

4. Start School Info:
docker build -t school-info .
docker run -p 8001:8001 --name school-info --env-file ../env/env-file.txt --net docker-microservice-network school-info

5. Start School Event:
docker build -t school-event .
docker run -p 8002:8002 --name school-event --env-file ../env/env-file.txt --net docker-microservice-network school-event