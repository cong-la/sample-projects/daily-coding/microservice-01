package com.simsoft.api.schoolinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class SchoolInfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolInfoApplication.class, args);
	}

}
